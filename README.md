![img](detectiveIcon.png)

# JGCLog

## OVERVIEW

JGCLog is a framework that can detect when an app is running without Xcode and create a file with all the contents of the console log and send it by mail.
JGCLog can also detect a crash in the application and report it.

## INSTALATION

- XCODE 13.2.1 & SWIFT 5.5.2
```
pod 'JGCLog', :git => 'https://gitlab.com/javilinGarcia/jgclog-framework.git', :tag => 'V1.6'
```
- XCODE 12.2 & SWIFT 5.3.1
```
pod 'JGCLog', :git => 'https://gitlab.com/javilinGarcia/jgclog-framework.git', :tag => 'V1.5.2'
```
- XCODE 12.0.1 & SWIFT 5.3
```
pod 'JGCLog', :git => 'https://gitlab.com/javilinGarcia/jgclog-framework.git', :tag => 'V1.5.1'
```
- XCODE 11 & SWIFT 5
```
pod 'JGCLog', :git => 'https://gitlab.com/javilinGarcia/jgclog-framework.git', :tag => 'V1.5.0'
```
- XCODE 10 & SWIFT 4.2
```
pod 'JGCLog', :git => 'https://gitlab.com/javilinGarcia/jgclog-framework.git', :tag => 'V1.4.1'
```


## USAGE

- Import JGCLog

- In appdelegate declare the following variable:

```
let uncaughtExceptionHandler: Void = NSSetUncaughtExceptionHandler { (exception) in

    UserDefaults.standard.set(true, forKey: "ExceptionHandler")
    UserDefaults.standard.synchronize()

    NSLog("Name:" + exception.name.rawValue)

    if exception.reason == nil {
        print("Reason: nil")
    } else {
        print("Reason: \(exception.reason!)")
        print("Error Handling callStackSymbols: \(exception.callStackSymbols.joined(separator: "\n"))")
    }

}
```
- In didFinishLaunchingWithOptions method:

```
JGCLogManager.shared.startWith(delegate: self)

if let consoleButton = JGCLogManager.shared.getAccessButton() {
    window!.rootViewController!.view.addSubview(consoleButton)

    let constraints = NSMutableArray()
    constraints.addObjects(from: NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[consoleButton]", options: NSLayoutConstraint.FormatOptions(rawValue:0), metrics: nil, views:["consoleButton":consoleButton]))
    constraints.addObjects(from: NSLayoutConstraint.constraints(withVisualFormat: "V:[consoleButton]-10-|" , options: NSLayoutConstraint.FormatOptions(rawValue:0), metrics: nil, views: ["consoleButton":consoleButton]))
    constraints.add(NSLayoutConstraint.init(item: consoleButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 25.0))
    constraints.add(NSLayoutConstraint.init(item: consoleButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 25.0))

    consoleButton.superview?.addConstraints(constraints as! [NSLayoutConstraint])

    window!.bringSubviewToFront(consoleButton)
    window!.makeKeyAndVisible()

}
```
- And finally, add the JGCLogConsoleDelegate to navigate to console view:

```
extension AppDelegate: JGCLogConsoleDelegate {
    func navigateToLogConsoleViewController(logConsoleViewController: UIViewController) {
        let rootVC = window!.rootViewController
        rootVC!.present(logConsoleViewController, animated: true, completion: nil)
    }
}
```

## What's new in 1.6 version

- Build with Xcode 13.2.1 & Swift 5.5.2
- Minor bug fixing.
- Refactor lipo script to xcframework script
- Demo App added.

## Screenshots of demo app

![img|540×1170, 80%](Screenshot1.png)
![img|540×1170, 80%](Screenshot2.png)

## Known Issues

- In this version it is not possible to catch crashes and exceptions, we are working on it, we hope to solve it soon.

## CONTACT

Do you have any questions or idea? My email is: javilin.garcia@gmail.com.

## LICENSE

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
