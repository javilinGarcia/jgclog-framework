//
//  SceneDelegate.swift
//  demo
//
//  Created by Javier García on 29/12/21.
//

import JGCLog
import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    let uncaughtExceptionHandler: Void = NSSetUncaughtExceptionHandler { (exception) in
        UserDefaults.standard.set(true, forKey: "ExceptionHandler")
        UserDefaults.standard.synchronize()

        NSLog("Name:" + exception.name.rawValue)

        if exception.reason == nil {
            print("Reason: nil")
        } else {
            print("Reason: \(exception.reason!)")
            print("Error Handling callStackSymbols: \(exception.callStackSymbols.joined(separator: "\n"))")
        }

    }

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }

        JGCLogManager.shared.startWith(delegate: self)

        if let consoleButton = JGCLogManager.shared.getAccessButton() {
            window!.rootViewController!.view.addSubview(consoleButton)

            let constraints = NSMutableArray()
            constraints.addObjects(from: NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[consoleButton]", options: NSLayoutConstraint.FormatOptions(rawValue:0), metrics: nil, views:["consoleButton":consoleButton]))
            constraints.addObjects(from: NSLayoutConstraint.constraints(withVisualFormat: "V:[consoleButton]-10-|" , options: NSLayoutConstraint.FormatOptions(rawValue:0), metrics: nil, views: ["consoleButton":consoleButton]))
            constraints.add(NSLayoutConstraint.init(item: consoleButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 25.0))
            constraints.add(NSLayoutConstraint.init(item: consoleButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 25.0))

            consoleButton.superview?.addConstraints(constraints as! [NSLayoutConstraint])

            window!.bringSubviewToFront(consoleButton)
            window!.makeKeyAndVisible()

        }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

extension SceneDelegate: JGCLogConsoleDelegate {
    func navigateToLogConsoleViewController(logConsoleViewController: UIViewController) {
        let rootVC = window!.rootViewController
        rootVC!.present(logConsoleViewController, animated: true, completion: nil)
    }
}
