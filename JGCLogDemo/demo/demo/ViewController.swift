//
//  ViewController.swift
//  demo
//
//  Created by Javier García on 29/12/21.
//
import JGCLog
import UIKit

class ViewController: JGCViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewdidload - from app")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear - from app")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    @IBAction func crashButtonTapped(_ sender: Any) {
        print(#function)

        let array = NSArray()
        _ = array.object(at: 99)
    }


    // NOTE:- It does not catch any Swift 2 errors (from throw) or Swift runtime errors, so this is not caught:
    @IBAction func notCatchableErrorTapped(_ sender: Any) {
        print(#function)

         let arr = [1, 2, 3]
         let _ = arr[4]
    }

    @IBAction func nilObjetTapped(_ sender: Any) {
        print(#function)

        let num1: Int? = nil
        let _ = num1! + 1
    }
}


