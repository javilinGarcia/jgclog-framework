Pod::Spec.new do |s|
  s.name         = "JGCLog"
  s.version      = "1.6"
  s.summary      = "Debug log tracking Framework."
  s.license      = { :type => "MIT", :file => "FILE_LICENSE" }

  s.description  = <<-DESC
  
JGCLog is a framework that can detect when an app is running without Xcode and create a file with all the contents of the console log and send it by mail.
JGCLog can also detect a crash in the application and report it.

Tag 1.6 for Xcode 13.2.1 & Swfit 5.5.2

                   DESC
  s.homepage      = "https://gitlab.com/javilinGarcia"
  s.license       = "Copyright © 2018 Fco. Javier Garcia Castro. All rights reserved."
  s.author        = { "Fco. Javier García" => "javilin.garcia@gmail.com" }
  s.source        = { :git => 'https://gitlab.com/javilinGarcia/jgclog-framework.git', :tag => 'V1.6' }
  
  s.vendored_frameworks = 'JGCLog.xcframework'
  s.source_files        = 'JGCLog.xcframework/ios-arm64/Headers/*.{h}'
  s.public_header_files = 'JGCLog.xcframework/ios-arm64/Headers/*.{h}'

  s.platform      = :ios
  s.swift_version = '5'

end
